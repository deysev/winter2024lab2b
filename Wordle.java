import java.util.Scanner;
import java.util.Random;

/* 
* assignment 3 2023 - 110
* 
* program to create a game called Wordle 
* where a five-letter word has to be 
* guessed and colors of letters in guessed
* word change according to position or presence
* @author Deyanira Sevilla id:2341931
* @version 2023-12
*/ 

public class Wordle {
	/*public static void main(String[] args) {
	String word = generateWord();
	runGame(word);	
	} */

	/*
	* method that randomly generates a word
	* from an array which will represent the word
	* needed to be guessed
	*/
	public static String generateWord() {
		// 20 5-letter words
		String[] wordList = {"NIGHT", "DOUGH", "ADOPT", "ADMIT", "VIXEN", "FROST",
		"BLING", "JUMPY","BRICK", "FEAST", "CLOWN", "DANCE", "TOWEL",
		"CLOTH","LUMPY", "CHALK", "THIEF", "VYNIL", "FORKS", "KINGS"};
		Random randomGen = new Random();
		return wordList[randomGen.nextInt(wordList.length)];
	}
	
    /*
	* method to check if letters in guessed word by 
	* player are in the generated word
	*
	* return true if it's in word
	* return false if it's not in word
	*/
	public static boolean letterInWord(String word, char letter) {
		for (int i = 0; i < word.length(); i++) {
			if (word.charAt(i) == (letter)) {
				return true;
			}
		}
		return false;
	} 
	
	/*
	* method to check if letters in guessed word by player 
	* are in the same spot as the letters in generated word
	* 
	* ex. generated word:"STRAW" guessed word letter:"T" position in guessed word: "1" = true
	* ex. generated word:"STRAW" guessed word letter:"T" position in guessed word: "2" = false
	*
	* return true if letter is in corresponding position
	* return false if letter is not in corresponding position
	*/
	public static boolean letterInSlot(String word, char letter, int position) {
			if (word.charAt(position) == letter) {
				return true;
			}

		return false;
	} 
	
	/*
	* method to fill an array with colours
	* which will determine if letters in guessed word
	* are in the genereated word and in correct spot
	
	* letters in word and correct spot = green
	* letters in word but incorrect spot = yellow
	* letters not in word = white
	* return array of colours
	*
	* ex. generated word = "STRAW"
	*     guessed word by player = "WORST"
	      return {yellow, white, green, yellow, yellow}
	*/
	public static String [] guessWord(String answer, String guess) {
		String[] colours = new String[5];
		for (int i = 0; i < 5; i++) {
			char guessedLetter = guess.charAt(i);
			if (letterInWord(answer, guessedLetter)) {  
				if (letterInSlot(answer, guessedLetter, i)) {
					colours[i] = "green";
				} else {
					colours[i] = "yellow";
				}
			} else {
				colours[i] = "white";
			}
		}
		return colours;
	}
	
	/* 
	* method that loops through array of colours
	* to turn the letters in guessed word
	* into the colour written in the array
	*/
	public static void presentResults (String guess, String[] colours) {
		for (int i = 0; i < 5; i++) {
			if (colours[i].equals("green")) {
				System.out.print("\u001B[32m" + guess.charAt(i) + "\u001B[0m");
			} else if (colours[i].equals("yellow")) {
				System.out.print("\u001B[33m" + guess.charAt(i) + "\u001B[0m");
			} else {
				System.out.print("\u001B[0m" + guess.charAt(i) + "\u001B[0m");
			}
		}
		System.out.println();
	}
	
	/*
	* method to ensure guessed word by player is five letters
	*
	* if not, ask player again until five letter word is input
	*
	* then turn guessed word into uppercase letters so that it can be compared
	* to generated word
	*/
	public static String readGuess() {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		String guess;
		do {
			System.out.println("Guess a five letter word: ");
			guess = reader.next();
			
			if (guess.length() != 5) {
				System.out.println("Word does not follow requirements. Please enter a five letter word: ");
			}
		} while (guess.length() != 5);
	
		guess = guess.toUpperCase();
		return guess;
	}
	
	/*
	* given the generated word
	* create a loop that only allows six attempts to guess the generated word
	* use other methods to determine if you win, else if attempts surpassed,
	* then try again
	*/
	public static void runGame(String word) {
		int count = 0;
		while (count < 6) {
			String guess = readGuess();
			String[] colours = guessWord(word, guess);
			presentResults(guess, colours);
			
			if (word.equals(guess)) {
				System.out.println("you win!");
				return;
			} else if (count == 5){
				System.out.println("Try again...");
			}
			count++;
		}
		return;
	}
}