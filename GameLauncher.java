import java.util.Scanner;

public class GameLauncher {
	public static void main (String [] args) {
		System.out.println("Hello and welcome to game launcher!");
		System.out.println("Enter '1' to play Wordle or '2' to play Hangman");
		java.util.Scanner reader = new java.util.Scanner(System.in);
		int userOption = reader.nextInt();
		reader.nextLine();
		//Launch Wordle
		if (userOption == 1) {
			String word = Wordle.generateWord();
			Wordle.runGame(word);
			
		//Launch Hangman
		} else {
			//Get user input
			System.out.println("Enter a 4-letter word:");
			String word = reader.next().toUpperCase();		
			//Start hangman game
			Hangman.runGame(word);
		}
	}
}
